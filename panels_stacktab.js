/**
 * @file
 * Client-side script for the panels_stacktab module.
 */

(function ($) {

  'use strict';

  /**
   * Find a tab panel given its ID.
   *
   * @param {string} id
   *   The ID (currently, HTML ID) of the tab panel to find.
   *
   * @return {jQuery}
   *   The first tab panel with the given ID.
   */
  Drupal.panelsStacktabFindTabPanel = function (id) {
    var $answer;
    $answer = $('#' + id + '[role="tabpanel"]').first();
    return $answer;
  };

  /**
   * Show a tab and its corresponding tabpanel, given a tab element.
   *
   * @param {HTMLElement} tabElement
   *   The tab element to show.
   */
  Drupal.panelsStacktabShow = function (tabElement) {
    var $tabelement;
    var $tabPanelElement;

    $tabelement = $(tabElement);
    $tabPanelElement = Drupal.panelsStacktabFindTabPanel($tabelement.attr('aria-controls'));

    // Set appropriate HTML attributes on the tab, tabpanel.
    $tabelement.attr({
      'aria-selected': 'true',
      'tabindex': '0'
    }).addClass('active');
    $tabPanelElement.removeAttr('aria-hidden');

    $tabelement.focus();
  };

  /**
   * Hide a tab and its corresponding tabpanel, given a tab element.
   *
   * @param {HTMLElement} tabElement
   *   The tab element to hide.
   */
  Drupal.panelsStacktabHide = function (tabElement) {
    var $tabelement;
    var $tabPanelElement;

    $tabelement = $(tabElement);
    $tabPanelElement = Drupal.panelsStacktabFindTabPanel($tabelement.attr('aria-controls'));

    // Set appropriate HTML attributes on the tab, tabpanel.
    $tabelement.attr('tabindex', '-1').removeAttr('aria-selected')
      .removeClass('active');
    $tabPanelElement.attr('aria-hidden', 'true');
  };

  /**
   * Handle a media change event.
   *
   * @param {string} stack_media_query
   *   The media query to use to enable stack mode.
   * @param {string} tab_media_query
   *   The media query to use to enable tab mode.
   * @param {jQuery} $container
   *   The container element to change.
   */
  Drupal.panelsStacktabHandleMediaChangeEvent = function (stack_media_query, tab_media_query, $container) {
    if (window.matchMedia(stack_media_query).matches) {
      $container.removeClass('tabmode');
      $container.addClass('stackmode');
    }
    else if (window.matchMedia(tab_media_query).matches) {
      $container.removeClass('stackmode');
      $container.addClass('tabmode');
    }
  };

  Drupal.behaviors.panels_stacktab = {
    attach: function (context, settings) {
      $('.panels_stacktab', context)
      // Tab setup and event handlers.
        .once('panels_stacktab_tabify', function () {
          var $tabs;

          $tabs = $('[role="tablist"] [role="tab"]', this);

          // Initial setup: hide all tabs, then show the first tab.
          $tabs.once('panels_stacktab_setup').each(function (index) {
            Drupal.panelsStacktabHide($(this));
          });
          Drupal.panelsStacktabShow($tabs.first());

          // Event handler: user uses keyboard to switch tabs.
          $tabs.keydown(function (e) {
            var $originalTab;
            var $targetTab;

            $originalTab = $(this);

            // Determine the target.
            switch (e.keyCode) {
              case 37:
                $targetTab = $(this).parents('li').prev().children('[role="tab"]');
                break;

              case 39:
                $targetTab = $(this).parents('li').next().children('[role="tab"]');
                break;

              default:
                $targetTab = false;
                break;
            }

            // If we have a target, switch tabs.
            if ($targetTab.length) {
              Drupal.panelsStacktabHide($originalTab);
              Drupal.panelsStacktabShow($targetTab);
            }

            // Finally, prevent the default action.
            e.preventDefault();
          });

          // Event handler: user uses click to switch tabs.
          $tabs.click(function (e) {
            var $targetTab;

            $targetTab = $(this);

            // Hide all tabs.
            $tabs.each(function (index) {
              Drupal.panelsStacktabHide($tabs[index]);
            });

            // Show the current tab.
            Drupal.panelsStacktabShow($targetTab);

            // Finally, prevent the default action.
            e.preventDefault();
          });
        })
        // Tab/stack mode change setup and event handlers.
        .once('panels_stacktab_modechange', function () {
          var $container;
          var tabset_id;
          var stack_media_query;
          var tab_media_query;

          $container = $(this);
          tabset_id = $container.attr('data-tabset_id');
          stack_media_query = Drupal.settings.panels_stacktab[tabset_id].stack_media_query;
          tab_media_query = Drupal.settings.panels_stacktab[tabset_id].tab_media_query;

          // Initial setup: set the container to stack mode.
          $container.addClass('stackmode');

          // Event handler: user changes device orientation.
          window.addEventListener('orientationchange', function () {
            Drupal.panelsStacktabHandleMediaChangeEvent(stack_media_query, tab_media_query, $container);
          }, false);

          // Event handler: user resizes window.
          window.addEventListener('resize', function () {
            Drupal.panelsStacktabHandleMediaChangeEvent(stack_media_query, tab_media_query, $container);
          }, false);
        });
    }
  };

})(jQuery);
