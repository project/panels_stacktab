<?php

/**
 * @file
 * Displays a set of tab titles and content.
 *
 * Available variables:
 * - $tabset_id: A unique identifier for this tabset.
 * - $titles: An array of tab title strings.
 * - $contents: An array of tab content strings.
 *
 * @see template_preprocess_panels_stacktab_tabs()
 *
 * @ingroup themeable
 */
?>
<div class="panels_stacktab" data-tabset_id="<?php print $tabset_id; ?>">
  <div class="tabs">
    <ul role="tablist" class="tabs primary">
      <?php foreach ($titles as $title): ?>
        <?php print $title; ?>
      <?php endforeach; ?>
    </ul>
  </div>

  <?php foreach ($contents as $content): ?>
    <?php print $content; ?>
  <?php endforeach; ?>
</div>
