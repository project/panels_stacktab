<?php

/**
 * @file
 * Displays a single tab's content.
 *
 * Available variables:
 * - $unique_id: A string containing the ID of the HTML element on this page
 *   which this tab refers to.
 * - $content: A string or array representing the content of this tab.
 *
 * @see template_preprocess_panels_stacktab_tab_content()
 *
 * @ingroup themeable
 */
?>
<section id="<?php print $unique_id; ?>" role="tabpanel">
  <?php print $content; ?>
</section>
