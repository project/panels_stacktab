<?php

/**
 * @file
 * Theme functions for panels_stacktab.module.
 */

/**
 * Prepares variables for a set of tab titles and content.
 *
 * Default template: panels-stacktab-tabs.tpl.php.
 *
 * @param array &$variables
 *   An associative array.
 */
function template_preprocess_panels_stacktab_tabs(array &$variables) {
  $module_path = drupal_get_path('module', 'panels_stacktab');

  // Add this module's base CSS and JS.
  drupal_add_css($module_path . '/panels_stacktab.css');
  drupal_add_js($module_path . '/panels_stacktab.js');
}

/**
 * Prepares variables for a single tab's title.
 *
 * Default template: panels-stacktab-tab-title.tpl.php.
 *
 * @param array &$variables
 *   An associative array containing:
 *   - unique_id: A string containing a unique identifier for the tab.
 *   - default_tab: A boolean indicating whether the tab is the default tab.
 */
function template_preprocess_panels_stacktab_tab_title(array &$variables) {
  // Generate a link given the unique ID.
  $variables['link_href'] = '#' . $variables['unique_id'];
}

/**
 * Prepares variables for a single tab's content.
 *
 * Default template: panels-stacktab-tab-content.tpl.php.
 *
 * @param array &$variables
 *   An associative array.
 */
function template_preprocess_panels_stacktab_tab_content(array &$variables) {
  // No-op.
}
