CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Troubleshooting
* FAQ

INTRODUCTION
------------

This module defines a Panels pane style which changes from stacked to tabbed as
the screen width shrinks.

REQUIREMENTS
------------

The Panels module (https://www.drupal.org/project/panels), version 7.x-3.0 or
higher.

RECOMMENDED MODULES
-------------------

The authors of this module made every effort to reduce the amount of markup that
this module outputs. However, if you wish to reduce the markup output by the
Panels panes and layouts, the Clean Markup module
(https://www.drupal.org/project/clean_markup) can be used to do so.

INSTALLATION
------------

1. Download and install the panels_stacktab project and its dependencies:

   See https://www.drupal.org/node/895232 for more information.

2. Edit a panel, and change a Panels region style to "Stacked-tabbed" by
   clicking the gear in the top-left corner of a region, then click "Change"
   under "Style", or click "Display settings" on the panel itself.

   When prompted, enter media queries to trigger the stacked and tabbed modes.
   Note that if you do not set up each media query to trigger when the other
   becomes false, then the element will remain in its previous state until the
   other media query triggers.

   Ensure that there are at least two Panels panes in the region.

TROUBLESHOOTING
--------------

We don't know of any problems at this time, so if you find one, please let us
know by adding an issue!

MAINTAINERS
-----------

Current maintainers:

 * M Parker (mparker17) - https://www.drupal.org/u/mparker17

This project has been sponsored by:

 * Digital Echidna - https://www.drupal.org/digital-echidna
