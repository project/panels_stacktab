<?php

/**
 * @file
 * Stacked-tabbed panels style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Stacked-tabbed'),
  'description' => t('A style which changes from stacked to tabbed as screen width shrinks.'),
  'render region' => 'panels_stacktab_stacked_tabbed_region',
  'settings form' => 'panels_stacktab_stacked_tabbed_region_settings_form',
);

/**
 * Form constructor for the stacked-tabbed pane settings form.
 *
 * @param array $conf
 *   An associative array containing:
 *   - panels_stacktab (optional): An associative array containing:
 *     - stack_media_query (optional): A string containing a media query to
 *       trigger stack mode.
 *     - tab_media_query (optional): A string containing a media query to
 *       trigger tab mode.
 */
function panels_stacktab_stacked_tabbed_region_settings_form(array $conf) {
  $settings = array();
  $form = array();

  // Set defaults.
  if (isset($conf['panels_stacktab'])) {
    $settings = $conf['panels_stacktab'];
  }
  $settings += array(
    'stack_media_query' => 'all and (min-width: 480px)',
    'tab_media_query' => 'all and (max-width: 479px)',
  );

  // Prompt the user for the media query to trigger stack mode.
  $form['panels_stacktab']['stack_media_query'] = array(
    '#type' => 'textfield',
    '#title' => t('Stack mode media query'),
    '#description' => t('A CSS media query which will trigger stack mode when it becomes true, for example, <code>@example</code>.', array(
      '@example' => '(orientation: portrait)',
    )),
    '#default_value' => $settings['stack_media_query'],
  );

  // Prompt the user for the media query to trigger tab mode.
  $form['panels_stacktab']['tab_media_query'] = array(
    '#type' => 'textfield',
    '#title' => t('Tab mode media query'),
    '#description' => t('A CSS media query which will trigger tab mode when it becomes true, for example, <code>@example</code>.', array(
      '@example' => '(orientation: landscape)',
    )),
    '#default_value' => $settings['tab_media_query'],
  );

  // Remind the user to check whether the two media queries make sense together.
  $form['panels_stacktab']['check_relationship'] = array(
    '#type' => 'item',
    '#description' => t('If you do not set up each media query to trigger when the other becomes false, then the element will remain in its previous state until the other media query triggers.'),
  );

  return $form;
}

/**
 * Prepares variables for stacked-tabbed panels regions.
 *
 * Default template: panels-stacktab-stacked-tabbed-region.tpl.php.
 *
 * @param array $variables
 *   An associative array containing:
 *   - display: An object containing information about the panels pane.
 *   - panes: An array containing the rendered panel panes.
 */
function template_preprocess_panels_stacktab_stacked_tabbed_region(array &$variables) {
  $variables['tab_title_array'] = array();
  $variables['tab_content_array'] = array();
  $variables['tabset_id'] = drupal_html_id('panels_stacktab_' . $variables['display']->uuid);
  $first = TRUE;

  foreach ($variables['display']->content as $internal_pane_id => $pane) {
    if (!$pane->shown) {
      continue;
    }
    $unique_id = drupal_html_id($variables['tabset_id'] . '-' . $internal_pane_id);

    $title = _panels_stacktab_determine_pane_title($pane);

    $variables['tab_title_array'][] = theme('panels_stacktab_tab_title', array(
      'unique_id' => $unique_id,
      'title' => $title,
      'default_tab' => $first,
    ));
    $variables['tab_content_array'][] = theme('panels_stacktab_tab_content', array(
      'unique_id' => $unique_id,
      'content' => $variables['panes'][$internal_pane_id],
      'default_tab' => $first,
    ));

    // On the first loop through the array, set $first = FALSE so that only the
    // first item is marked as the default tab.
    $first = FALSE;
  }
}

/**
 * Returns HTML for a stacked-tabbed panels region.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tab_title_array: An array of tab title list items, already marked up.
 *   - tab_content_array: An array of tab content items, already marked up.
 *
 * @ingroup themable
 */
function theme_panels_stacktab_stacked_tabbed_region(array &$variables) {
  $tabset_id = $variables['tabset_id'];
  $settings_array['panels_stacktab'][$tabset_id] = array(
    'stack_media_query' => '',
    'tab_media_query' => '',
  );

  // Prepare settings to forward to the front-end.
  if (isset($variables['settings']['panels_stacktab'])) {
    $settings_array['panels_stacktab'][$tabset_id]['stack_media_query'] = $variables['settings']['panels_stacktab']['stack_media_query'];
    $settings_array['panels_stacktab'][$tabset_id]['tab_media_query'] = $variables['settings']['panels_stacktab']['tab_media_query'];
  }

  drupal_add_js($settings_array, 'setting');
  return theme('panels_stacktab_tabs', array(
    'tabset_id' => $tabset_id,
    'titles' => $variables['tab_title_array'],
    'contents' => $variables['tab_content_array'],
  ));
}
