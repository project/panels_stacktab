<?php

/**
 * @file
 * Displays a single tab's title.
 *
 * Available variables:
 * - $link_href: A string containing the URL of the HTML element on this page
 *   which this tab refers to.
 * - $unique_id: A string containing the ID of the HTML element on this page
 *   which this tab refers to.
 * - $title: A string representing the title of this tab.
 *
 * @see template_preprocess_panels_stacktab_tab_title()
 *
 * @ingroup themeable
 */
?>
<li role="presentation">
  <a href="<?php print $link_href; ?>" role="tab" aria-controls="<?php print $unique_id; ?>">
    <?php print $title; ?>
  </a>
</li>
